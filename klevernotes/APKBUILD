# Contributor: Peter Mack <peter@linmob.net>
# Maintainer: Peter Mack <peter@linmob.net>

pkgname=klevernotes
pkgver=1.0.0
pkgrel=0
pkgdesc="A simple markdown note management app"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://apps.kde.org/marknote/"
license="GPL-3.0-or-later AND CC0-1.0"
depends="
	kirigami
	kirigami-addons
	kitemmodels
	qqc2-desktop-style
	"
makedepends="
	extra-cmake-modules
	kcolorscheme-dev
	kconfig-dev
	kcoreaddons-dev
	ki18n-dev
	kio-dev
	kirigami-addons-dev
	kirigami-dev
	kmime-dev
	kxmlgui-dev
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	qt6-qtsvg-dev
	qt6-qtwebengine-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/office/klevernotes.git"
source="https://download.kde.org/stable/$pkgname/$pkgver/$pkgname-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6cc1d0e789175d40142e3ae4ab201ca889c62f0da907776e15ba0de1858f306a1abbe22ae059c8a1c9357614ab8898633aad7914609a3f055338efddbe29c402  klevernotes-1.0.0.tar.xz
"
